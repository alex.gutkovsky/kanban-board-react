// Intial store
import { configureStore } from '@reduxjs/toolkit';
import stateReducer from './stateSlice'; // подключаем блок описания хранилища

export default configureStore({
    reducer: {
        state: stateReducer
    }
});