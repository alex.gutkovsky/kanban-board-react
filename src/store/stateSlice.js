// создаем редьюсер или блок описания для хранилища данныйх tasks
// импортирую в начальное состоятние хранилища объект из localStorage если там что-то есть
// создаем редьюсер или блок описания 

import { createSlice } from '@reduxjs/toolkit';

export const stateSlice = createSlice({
    name: 'tasks',
    initialState: localStorage.getItem('state') ? JSON.parse(localStorage.getItem('state')) : {
        "tasksToDo": [],
        "tasksPending": [],
        "tasksDone": [],
        "lastTaskId": 0
    },
    reducers: {
        create: (state, data) => {
            state['tasksToDo'].push(data.payload);
            if (state['lastTaskId'] !== 1) {
                state['tasksToDo'].sort((a, b) => a.priority < b.priority ? 1 : -1);
            }
            state['lastTaskId']++;
            localStorage.setItem('state', JSON.stringify(state))
        },
        next: (state, data) => { //реализовать логику между всеми тасками
            let indexOfTask = state[data.payload['moveFrom']].findIndex(task => task.id === data.payload['id']);
            if (data.payload['moveFrom'] === 'tasksToDo') {
                state['tasksPending'].push(state[data.payload['moveFrom']][indexOfTask]);
                state['tasksPending'].sort((a, b) => a.priority < b.priority ? 1 : -1);
                state[data.payload['moveFrom']].splice(indexOfTask, 1);
            }
            if (data.payload['moveFrom'] === 'tasksPending') {
                state['tasksDone'].push(state[data.payload['moveFrom']][indexOfTask]);
                state[data.payload['moveFrom']].splice(indexOfTask, 1);
            }
            localStorage.setItem('state', JSON.stringify(state))
        },
        remove: (state, data) => {
            console.log(data);
            let indexOfTask = state[data.payload['removeFrom']].findIndex(task => task.id === data.payload['id']);
            if (indexOfTask !== -1) {
                state[data.payload['removeFrom']].splice(indexOfTask, 1);
            }
            localStorage.setItem('state', JSON.stringify(state))
        }
    }
});

export const { create, next, remove } = stateSlice.actions; // reducers описанные выше
export const selectState = state => state;
export default stateSlice.reducer;