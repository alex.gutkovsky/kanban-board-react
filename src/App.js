import CreateTask from "./containers/createTask";
import TasksList from "./components/TasksList";



function App() {
  return (
    <div className='container'>
      <h1>Kanban Board</h1>
      <CreateTask />
      <TasksList />
    </div>
  );
}

export default App;
