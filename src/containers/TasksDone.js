import React from 'react';
import { useSelector } from 'react-redux';
import {
    selectState
} from '../store/stateSlice';
import { useEffect } from 'react';

export default function TasksDone() {
    const mapValueToPriority = {
        1: 'Low',
        2: 'Medium',
        3: 'High'
    }
    const tasks = useSelector(selectState); //tasks.state['tasksDone'] хранилище

    useEffect(() => {
        let footerTasksAmount = document.getElementById('countDone');

        if (tasks.state['tasksDone'].length === 1 || tasks.state['tasksDone'].length === 0) {
            footerTasksAmount.innerHTML = tasks.state['tasksDone'].length + ' item';
        } else footerTasksAmount.innerHTML = tasks.state['tasksDone'].length + ' items';
    })

    return (
        <div className="col">
            <div className="card">
                <div className="card-header">
                    DONE
                </div>
                <div className="card-body" id="containerDone">
                    {tasks.state['tasksDone'].map(item => (
                        <div className="border border-2 p-2 position-relative mt-2" key={item.id}>
                            <div id="taskContent" className="mt-4 mb-5">
                                <h5 className="card-title">{item['summary']}</h5>
                                <p className="card-text">{mapValueToPriority[item['priority']]}</p>
                                <p className="card-text">{item['description']}</p>
                            </div>
                        </div >
                    )
                    )}
                </div>
                <div className="card-footer text-muted" id="countDone">0 item</div>
            </div>
        </div>
    )
}