import { useSelector, useDispatch } from 'react-redux';
import { create } from '../store/stateSlice'
import {
    selectState
} from '../store/stateSlice';

export default function CreateTask() {
    const tasks = useSelector(selectState); //tasks.state[''] хранилище
    const dispatch = useDispatch();

    function addTask() {
        let summary = document.getElementById('summaryInput').value;
        let priority = +document.getElementById('prioritySelect').value;
        let description = document.getElementById('descriptionInput').value;

        let newTask = {
            'id': tasks.state['lastTaskId'],
            'summary': summary,
            'priority': priority,
            'description': description
        }

        dispatch(create(newTask))
    }

    return (
        <div className="row">
            <div className="col-6">
                {/* <!-- Button trigger modal --> */}
                <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
                    Create
                </button>

                {/* <!-- Modal --> */}
                <div className="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content" >
                            <div className="modal-header">
                                <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div className="modal-body">
                                <form>
                                    <div className="mb-3">
                                        <label htmlFor="summaryInput" className="form-label">Summary</label>
                                        <input type="text" className="form-control" id="summaryInput" defaultValue="Test" required />
                                    </div>
                                    <div className="mb-3">
                                        <label htmlFor="prioritySelect" className="form-label">Priority</label>
                                        <select className="form-select" id="prioritySelect" required>
                                            <option value="1">Low</option>
                                            <option value="2">Medium</option>
                                            <option value="3">High</option>
                                        </select>
                                    </div>
                                    <div className="mb-3">
                                        <label htmlFor="descriptionInput" className="form-label">Description</label>
                                        <input type="text" className="form-control" defaultValue="Lorem ipsum dolor sit amet, consectetur " id="descriptionInput" required />
                                    </div>
                                </form>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                <button type="button" onClick={addTask} className="btn btn-primary float-end" id='createButton' data-bs-dismiss="modal">Create</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}