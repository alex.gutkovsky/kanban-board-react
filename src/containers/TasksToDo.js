import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
    selectState
} from '../store/stateSlice';
import { remove, next } from '../store/stateSlice'

export default function TasksToDo() {
    const tasks = useSelector(selectState); //tasks.state['tasksToDo'] хранилище
    const dispatch = useDispatch();
    const mapValueToPriority = {
        1: 'Low',
        2: 'Medium',
        3: 'High'
    }

    useEffect(() => {
        let footerTasksAmount = document.getElementById('countToDo');

        if (tasks.state['tasksToDo'].length === 1 || tasks.state['tasksToDo'].length === 0) {
            footerTasksAmount.innerHTML = tasks.state['tasksToDo'].length + ' item';
        } else footerTasksAmount.innerHTML = tasks.state['tasksToDo'].length + ' items';
    })

    function closeTask(id, removeFrom) {
        let data = {
            'id': id,
            'removeFrom': removeFrom
        }
        dispatch(remove(data))
    }

    function moveToDone(id, moveFrom) {
        let data = {
            'id': id,
            'moveFrom': moveFrom
        }
        dispatch(next(data))
    }
    return (
        <div className="col">
            <div className="card">
                <div className="card-header">
                    TODO
                </div>
                <div className="card-body" id="containerToDo">
                    {tasks.state['tasksToDo'].map(item => (
                        <div className="border border-2 p-2 position-relative mt-2" id={item['id']} draggable={true} key={item.id}>
                            <button
                                type="button"
                                onClick={() => closeTask(item['id'], 'tasksToDo')}
                                className="btn-close position-absolute top-0 end-0 m-2"
                                aria-label="Close">
                            </button>
                            <div id="taskContent" className="mt-4 mb-5">
                                <h5 className="card-title">{item['summary']}</h5>
                                <p className="card-text">{mapValueToPriority[item['priority']]}</p>
                                <p className="card-text">{item['description']}</p>
                            </div>
                            <button
                                type="submit"
                                onClick={() => moveToDone(item['id'], 'tasksToDo')}
                                className="btn btn-success position-absolute bottom-0 end-0 m-2"
                                id='nextButton'>
                                Next
                            </button>
                        </div >
                    )
                    )}
                </div>
                <div className="card-footer text-muted" id="countToDo">0 item</div>
            </div>
        </div>
    )
}