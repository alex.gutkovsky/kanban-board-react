// получает данные из контейнера и выводит на экран

import React from 'react';
import TasksToDo from '../containers/TasksToDo';
import TasksPending from '../containers/TasksPending';
import TasksDone from '../containers/TasksDone';

export default function TasksList() {
    return (
        <>
            <div className="row py-4">
                <TasksToDo />
                <TasksPending />
                <TasksDone />
            </div>
        </>
    )
}